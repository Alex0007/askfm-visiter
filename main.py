# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
import time
import re
import random
import os
import threading

import vk_api
import requests


# in F12 mode in Chrome catch requst from https://vk.com/app4392060#askfm&aleksashaomg
# to that site and remove 'aleksashaomg' from back

url = os.environ.get('ASKFM_LINK')
print('ASKFM_LINK: ', url)

cookies = None
headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.94 Safari/537.36'}

vk = vk_api.VkApi()


def search_askfm(startfrom=''):
    global next_from, again
    askfm_ids = []
    values = {'q': 'askfm', 'count': 200 , 'start_from': startfrom}
    response = vk.method('newsfeed.search', values)
    try:
        next_from = response['next_from']
    except:
        again = True
    for item in response['items']:
        try:
            to_append = str.split(item['attachments'][0]['link']['url'], '/')[-1]
        except:
            to_append = str.split(item['text'], '/')[-1]
            to_append = str.split(to_append, ' ')[0]
        askfm_ids.append(to_append)
    return askfm_ids



def go_askfm(ids):
    for item in ids:
        try:
            r = requests.get(url + item)
            print(str(item) + ' visited. Status: ' + str(r.status_code) + ' ' + str(datetime.now()))
        except:
            print('some exception')
            pass


def start_threads(ids):
    try:
        threading.Thread(target=go_askfm, args=(ids[:50],)).start()
        threading.Thread(target=go_askfm, args=(ids[50:100],)).start()
        threading.Thread(target=go_askfm, args=(ids[100:150],)).start()
        threading.Thread(target=go_askfm, args=(ids[150:],)).start()
    except:
        print("Error: unable to start thread")

def get_auth_token():
    global cookies, headers
    resp = requests.get('http://ask.fm/', headers=headers)
    cookies = resp.cookies
    result = re.findall(r'\'.*\'',
                        re.findall(r'encodeURIComponent\(\'.*\'\)',
                                   resp.text)[0])[0].replace('\'', '')
    return result


token = get_auth_token()

i = 0
next_from = ''
again = False

while True:
    if not again:
        print('i=', i, 'nextfrom=', next_from)
        start_threads(search_askfm(next_from))
        i += 1
    else:
        i = 0
        next_from = ''
        print('waiting now')
        time.sleep(1200 + random.randint(0, 600))
        again = False
